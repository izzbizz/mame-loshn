#!/usr/bin/env python
# -*- coding: utf-8 -*-

import glob
import pandas as pd
import unicodedata
import re
import json
import sys

YIVO_SOURCES = ['nouns', 'proverbs', 'refoyl', 'songs', 'verterbukh', 'wiktionary', 'yiddishland']

def decompose_tsvey_yudn(s):
 	return s.replace('ײ', 'יי')
 	
def decompose_tsvey_vovn(s):
	return s.replace('װ', 'וו')
	
def decompose_vov_yud(s):
	return s.replace('ױ', 'וי')
	
def compose_tsvey_yudn(s):
	'''compose all instances of yud + yud except at the beginning of words''' 
	words = s.split()
	return ' '.join([w[0]+w[1:].replace('יי', 'ײ') for w in words])
	
def compose_tsvey_vovn(s):
	return s.replace('וו', 'װ')
	
def compose_vov_yud(s):
	'''compose all instances of vov + yud except at the beginning of words''' 
	words = s.split()
	return ' '.join([w[0]+w[1:].replace('וי', 'ױ') for w in words])

def decompose(s):
	'''Normalization Form Decomposition
	for transliteration & source string
	for source string: decompose Yiddish ligatures'''
	return unicodedata.normalize('NFD', s)

def decompose_ligatures(s):
	s = decompose_tsvey_yudn(s)
	s = decompose_tsvey_vovn(s)
	s = decompose_vov_yud(s)
	return s
	
def compose(s):
	'''compose Yiddish ligatures for letter combinations'''
	s = compose_tsvey_yudn(s)
	s = compose_tsvey_vovn(s)
	s = compose_vov_yud(s)	
	return s
	
def compose_only_vovn(s):
	s = decompose_tsvey_yudn(s)
	s = decompose_vov_yud(s)
	s = compose_tsvey_vovn(s)
	return s	

def combine_n_preprocess(files, outfile, compose_ligs='ligs'):
	'''turn files into one pandas df, unify orthography, mark yivo files, save as csv'''
	dfs = [pd.read_csv(f) for f in files]
	df = pd.concat(dfs)
	df = df[df.yiddish.notna() & df.transliteration.notna()]
	
	df['yiddish'] = df.yiddish.apply(lambda s: decompose(s))
	df['transliteration'] = df.transliteration.apply(lambda s: decompose(s))

	if compose_ligs == 'ligs':
		print('composing all three ligatures')
		df['yiddish'] = df.yiddish.apply(lambda s: compose(s))
	
	elif compose_ligs == 'only_vovn':
		print('composing only tsvey-vovn, decomposing all others')
		df['yiddish'] = df.yiddish.apply(lambda s: compose_only_vovn(s))
		
	elif compose_ligs == 'no':
		print('decomposing all ligatures')
		df['yiddish'] = df.yiddish.apply(lambda s: decompose_ligatures(s))
	
	else:
		print('not a valid option')

	df['yivo'] = df.source.apply(lambda x: x in YIVO_SOURCES)
	df[['yiddish', 'transliteration', 'source', 'yivo']].to_csv(outfile, index=False)

if __name__ == '__main__':
	inpath = sys.argv[1]
	outpath = sys.argv[2]
	compose_ligs = sys.argv[3]
	files = glob.glob(f'{inpath}/*.csv')
	combine_n_preprocess(files, outpath, compose_ligs=compose_ligs)
