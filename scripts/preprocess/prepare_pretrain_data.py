#!/usr/bin/env python
# -*- coding: utf-8 -*-

import glob
import re
import random
import sys
import pandas as pd

from standardize_ortho import unify_yiddish

WIKI_DIR = '../../../data/yiddish-wikipedia/AA'
YIDDISH_RANGE = '[\u0590-\u05FF]+'
LC_REGEX = '[\w\u05BE\u05BF]+'
OUTDIR = '../../../data/pretraining_data'

def convert2lc(word):
	'''strip diacritics from yivo words'''
	return ''.join(re.findall(LC_REGEX, word))
	
def delete_letter(word):
	'''noising function 1: delete random letter from word'''
	characters = list(word)
	if len(characters) > 0:
		random_index = random.choice(range(len(word)))
		characters[random_index] = '_'
	return ''.join(characters)
	
def insert_gap(word):
	'''noising function 2: insert random gap into word'''
	if len(word) > 0:
		random_index = random.choice(range(len(word)))
		word = word[:random_index] + '_' + word[random_index:]
	return word

def noise_wiki(outfile, lc=False, downsample=True):
	'''make noisy data for pretraining: exctract words from wiki corpus,
	keep original word as target, add noise to create source words'''
	files = glob.glob(f'{WIKI_DIR}/*')
	text = ''
	for file in files:
		with open(file) as f:
			text += f.read()
	words = re.findall(YIDDISH_RANGE, text)
	unique_words = list(set(words))
	df = pd.DataFrame({'target': unique_words})
	
	if lc:
		df['target'] = df.target.apply(lambda x: convert2lc(x))
		
	df['target'] = df.target.apply(lambda s: unify_yiddish(s))
		
	df = df[df.target.str.len() >= 2]
#	sample1 = df.sample(frac=.5)
#	sample2 = df.drop(sample1.index)
	df['source'] = df.target.apply(lambda x: delete_letter(x))
#	sample2['source'] = sample2.target.apply(lambda x: insert_gap(x))
	
#	df = pd.concat([sample1, sample2])
	
	if downsample:
		df = df.sample(n=40000)
	
	print(f'processed {len(df)} words, storing in {OUTDIR}/{outfile}')
		
	with open(f'{OUTDIR}/{outfile}', 'w') as f:
		for words, wordt in zip(df.source.values, df.target.values):
			f.write(f'{words}\t{wordt}\n')
	
if __name__ == '__main__':
	outfile = sys.argv[1]
	lc = bool(int(sys.argv[2]))
	downsample = bool(int(sys.argv[3]))
	noise_wiki(outfile, lc=lc, downsample=downsample)
