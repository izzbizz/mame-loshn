#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

class Scrambler():
	'''applies a random amount of noising functions to input data
	can be applied to sentences (entity --> sentence, unit__> word
	or to words (entity --> word, unit --> character)'''
	def __init__(self, vocab, mode='word'):
		self.mode = mode
		self.vocab = vocab
				
	def permutate_delete(self, entity):
		'''delete unit from entity'''
		if self.mode == 'word':
			units = entity.split()
			separator = ' '
		elif self.mode == 'char':
			units = list(entity)
			separator = ''
		if len(units) > 0:
			del units[random.choice(range(len(units)))]
		return separator.join(units)
	
	def permutate_replace(self, entity):
		'''replace unit with other random unit'''
		if self.mode == 'word':
			units = entity.split()
			separator = ' '
		elif self.mode == 'char':
			units = list(entity)
			separator = ''
		if len(units) > 0:
			units[random.choice(range(len(units)))] = random.choice(self.vocab)
		return separator.join(units)
	
	def permutate_swap(self, entity):
		'''swap two units in entity'''
		if self.mode == 'word':
			units = entity.split()
			separator = ' '
		elif self.mode == 'char':
			units = list(entity)
			separator = ''
		if len(units) > 1:
			choices = random.choices(range(len(units)), k=2)
			print(choices)
			unit1 = units[choices[0]]
			units[choices[0]] = units[choices[1]]
			units[choices[1]] = unit1
		return separator.join(units)
		
	def scramble(self, entity, n=4, swap=True, delete=True, replace=True):
		'''from the list of possible noising functions, apply a random selection of n functions to input'''
		functions = []
		if swap:
			functions.append(self.permutate_swap)
		if delete:
			functions.append(self.permutate_delete)
		if replace:
			functions.append(self.permutate_replace)
		chosen_functions = random.choices(functions, k=random.choice(range(n+1)))
		for function in chosen_functions:
			entity = function(entity)
		return entity
