#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re

YIDDISHRGX = '[\u0590-\u05FF\uFB2A-\uFB4E \-־–\']*'
HYPHENS = '[\-־–]'

def remove_punct(s):
    '''remove all punctuation except hyphens and apostrophes'''
    punct = '\!"#$%&\()*+,./:;<=>?@[\\]^_`{|}~'
    s = re.sub(fr'[{punct}]+', ' ', s).lower()
    s = s.replace('\\', ' ')
    s = re.sub(' +', ' ', s)
    return s.strip()
    
def extract_yiddish(s):
    '''extract only yiddish parts, get rid of comments etc.'''
    result = re.findall(YIDDISHRGX, s)
    if result != None:
        return ' '.join(result)
        
def remove_brackets(s):
    '''get rid of brackets and their content'''
    s = re.sub('\{.*?\}', '', s)
    s = re.sub('\(.*?\)', '', s)
    s = re.sub('\[.*?\]', '', s)
    return s

def remove_single_letters(s):
	s = re.sub(r'\b\w\b', ' ', s)
	return s

def apo(s):
	'''replace apostrophe with geresh'''
	s = s.replace("'", "׳")
	return s

def hyphen(s):
	'''replace non-yiddish hyphens with maqaf'''
	s = re.sub('[–-]', '־', s)
	return s

def remove_codes(s):
	'''remove leftover sign codes'''
	apostrophe = '&#039;'
	quotation1 = '&#8221;'
	quotation2 = '&#8220;'
	emdash = '&#8212;'
	s = re.sub(apostrophe, '', s)
	s = re.sub(quotation1, '', s)
	s = re.sub(quotation2, '', s)
	s = re.sub(emdash, '', s)
	return s
	
def remove_numbers(s):
	s = re.sub('\d+\-?־?\d*', '', s)
	return s
	
def normalize_whitespace(s):
	s = re.sub('\s+', ' ', s)
	return s

def remove_datl(s):
	s = re.sub('\&datl\=', '', s)
	return s
	
def split_hyphen(s):
	s = re.sub(f'{HYPHENS}', ' ', s)
	return s
