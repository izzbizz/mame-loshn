#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import re
import sys
from helpers import apo, hyphen, remove_datl, remove_punct, extract_yiddish, remove_codes, remove_numbers, split_hyphen, remove_brackets, normalize_whitespace

def prepare_input(f, source, outpath, filetype='csv', y='yiddish', t='transliteration', save=True, hypsplit=True, brackdel=False):
	if filetype == 'csv':
		df = pd.read_csv(f)
	elif filetype == 'excel':
		df = pd.read_excel(f)
	df = df[(df[y].notna() & df[t].notna())]
	
	df['yiddish'] = df[y].apply(lambda x: remove_datl(x))
	if brackdel:
		df['yiddish'] = df.yiddish.apply(lambda x: remove_brackets(x))
	df['yiddish'] = df.yiddish.apply(lambda x: apo(x))
	df['yiddish'] = df.yiddish.apply(lambda x: hyphen(x))
	df['yiddish'] = df.yiddish.apply(lambda x: remove_numbers(x))
	df['yiddish'] = df.yiddish.apply(lambda x: remove_punct(x))
	df['yiddish'] = df.yiddish.apply(lambda x: extract_yiddish(x))
	if hypsplit:
		df['yiddish'] = df.yiddish.apply(lambda x: split_hyphen(x))
	df['yiddish'] = df.yiddish.apply(lambda x: normalize_whitespace(x))
	
	df['transliteration'] = df[t].apply(lambda x: remove_datl(x))
	if brackdel:
		df['transliteration'] = df.transliteration.apply(lambda x: remove_brackets(x))
	df['transliteration'] = df.transliteration.apply(lambda x: remove_punct(x))
	df['transliteration'] = df.transliteration.apply(lambda x: remove_codes(x))
	df['transliteration'] = df.transliteration.apply(lambda x: remove_numbers(x))
	if hypsplit:
		df['transliteration'] = df.transliteration.apply(lambda x: split_hyphen(x))
	df['transliteration'] = df.transliteration.apply(lambda x: normalize_whitespace(x))
	
	df['source'] = source
	if save:
		df[['yiddish', 'transliteration', 'source']].to_csv(f'{outpath}/{source}.csv', index=False)
		return
	else:
		return df[['yiddish', 'transliteration', 'source']]
	
def prepare_nouns(inpath, outpath, hypsplit=False):
	fin = f'{inpath}/nouns/multi_orthography_parallel_corpus_of_yiddish_nouns.csv'
	fout = f'{outpath}/nouns.csv'
	df = pd.read_csv(fin)
	pairs = list(zip(df.yivo.values, df.romanized.values, ['nouns']*len(df)))
	pair_df = pd.DataFrame({'yiddish': [tup[0] for tup in pairs], 'transliteration': [tup[1] for tup in pairs], 'source': [tup[2] for tup in pairs]})
	if hypsplit:
		pair_df['yiddish'] = pair_df.yiddish.apply(lambda x: split_hyphen(x))
		pair_df['transliteration'] = pair_df.transliteration.apply(lambda x: split_hyphen(x))
	pair_df.drop_duplicates().to_csv(fout, index=False)

def prepare_ybc(inpath, outpath, hypsplit):
	fin=f'{inpath}/ybc/all-titles-ybc.csv'
	df = pd.read_csv(fin)
	title_df = df[['title', 'title-trans']]
	author_df = df[['author', 'author-trans']]
	title_df.columns = ['yiddish', 'transliteration']
	author_df.columns = ['yiddish', 'transliteration']
	df = pd.concat([title_df, author_df])
	df.to_csv(f'{inpath}/ybc/ybc.csv', index=False)
	prepare_input(f'{inpath}/ybc/ybc.csv', 'ybc', outpath, hypsplit=hypsplit)

	
def prepare_yiddishland(inpath, outpath, hypsplit):
	fin=f'{inpath}/yiddishland/yiddishland'
	fout=f'{outpath}/yiddishland.csv'
	with open(fin) as f:
		lines = f.readlines()[3:]
	triplets = [line.split() for line in lines if len(line.split()) == 3]
	df = pd.DataFrame({'yiddish': [trip[2] for trip in triplets],
		'transliteration': [trip[0] for trip in triplets]})
	df.to_csv(f'{inpath}/yiddishland/yiddishland.csv')
	df = prepare_input(f'{inpath}/yiddishland/yiddishland.csv', 'yiddishland', 'dummy', save=False, brackdel=True, hypsplit=hypsplit)
	df['transliteration'] = df.transliteration.apply(lambda x: re.sub('[ \']', '', x))
	df.to_csv(fout, index=False)
	
if __name__ == '__main__':
	inpath = sys.argv[1]
	outpath = sys.argv[2]
	hypsplit = bool(int(sys.argv[3]))
	prepare_input(f'{inpath}/brandeis/Yiddish titles export.xlsx', 'brandeis', outpath, filetype='excel', y='title', t='trans', hypsplit=hypsplit)
	prepare_input(f'{inpath}/wiktionary/wiktionary.csv', 'wiktionary', outpath, t='translit', hypsplit=hypsplit)
	prepare_input(f'{inpath}/songtexts/lider-verter.csv', 'songs', outpath, hypsplit=hypsplit)
	prepare_input(f'{inpath}/refoyl/wordlist.csv', 'refoyl', outpath, y='Yiddish', t='Romanized', hypsplit=hypsplit)
	prepare_input(f'{inpath}/proverbs/yiddish-wit.csv', 'proverbs', outpath, hypsplit=hypsplit)
	prepare_input(f'{inpath}/nli/nli.csv', 'nli', outpath, hypsplit=hypsplit)
	prepare_input(f'{inpath}/names/names.csv', 'names', outpath, t='english', hypsplit=hypsplit)
	prepare_input(f'{inpath}/hebis/hebis.csv', 'hebis', outpath, hypsplit=hypsplit)
	prepare_input(f'{inpath}/verterbukh/verterbukh.csv', 'verterbukh', outpath, hypsplit=hypsplit)
	prepare_nouns(inpath, outpath, hypsplit=hypsplit)
	prepare_ybc(inpath, outpath, hypsplit=hypsplit)
	prepare_yiddishland(inpath, outpath, hypsplit=hypsplit)
