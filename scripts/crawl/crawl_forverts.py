#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import re
from collections import defaultdict

def parse_urls(urls):
    dycty = defaultdict(lambda: defaultdict(str))
    for url in urls:
        page = requests.get(url)
        text = page.text
        title = re.findall('h1 itemprop=\"headline name\">(.*?)<|$', text)
        english_title = re.findall('h2>(.*?)<|$', text)
        author = re.findall('<meta name=\"search\.author\" content=\"(.*?)\">|$', text)
        date = re.findall('<meta name=\"date\" content=\"(.*?)\">|$', text)
        section = re.findall('<meta name=\"search\.section\" content=\"(.*?)\">|$', text)
        subsection = re.findall('<meta name=\"search\.subsection\" content=\"(.*?)\">|$', text)
        timestamp = re.findall('<meta name=\"search\.timestamp\" content=\"(.*?)\">|$', text)
        type_ = re.findall('<meta name=\"search\.type\" content=\"(.*?)\">|$', text)
        description = re.findall('<meta property=\"og:description\" content=\"(.*?)\">|$', text)
        article = re.findall('<div itemprop=\"articleBody\" class=\"article-content\">([\w\W]*?)<div|$', text)
        dictx = defaultdict(str)
        dictx['title'] = title[0]
        dictx['title_english'] = english_title[0]
        dictx['author'] = author[0]
        dictx['date'] = date[0]
        dictx['section'] = section[0]
        dictx['subsection'] = subsection[0]
        dictx['timestamp'] = timestamp[0]
        dictx['type'] = type_[0]
        dictx['description'] = description[0]
        dictx['article'] = article[0]
        dycty[url] = dictx

    return dycty

