#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import re
import time
import pandas as pd

all_books = 11707
pages = round(all_books / 10)
index_url = 'https://www.yiddishbookcenter.org/search/collection/"Yiddish%2520Book%2520Center%2527s%2520Spielberg%2520Digital%2520Yiddish%2520Library"?page='
meta_url = 'https://www.yiddishbookcenter.org/'

for page in range(pages):
	quartlets = []
	print(f'page {page}')
	url = index_url + str(page)
	bookpage = requests.get(url).text
	booklinks = re.findall('search\-result__field\-\-title.*?href="(.*?)"', bookpage)
	for link in booklinks:
		text = requests.get(meta_url + link).text
		title = re.findall('"article\-book__title" lang="yi"\>(.*?)\<', text)
		author = re.findall('"article\-book__author" lang="yi".*?link"\>(.*?)\<', text)
		ttitle = re.findall('"article\-book__title"\>(.*?)\<', text)
		tauthor = re.findall('"article-book__author"\>.*?link"\>(.*?)\<', text)
		quartlet = []
		for item in [title, author, ttitle, tauthor]:
			if item == []:
				quartlet.append('')
			else:
				quartlet.append(item[0])		
		quartlets.append(tuple(quartlet))
		time.sleep(1)
			
	titles = [tup[0] for tup in quartlets]
	authors = [tup[1] for tup in quartlets]
	trans_titles = [tup[2] for tup in quartlets]
	trans_authors = [tup[3] for tup in quartlets]
		
	df = pd.DataFrame({'title': titles, 'author': authors, 'title-trans': trans_titles, 'author-trans': trans_authors})
	df.to_csv(f'~/Desktop/mame-loyshn/data/dataYiddish/ybc/page_{page}.csv')
