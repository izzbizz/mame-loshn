#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import re
import pandas as pd

def parsewords():
	yiddishstrings = []
	translitstrings = []

	for letter in list('abdefghiklmnoprstuvyz'):
		with open(f'/~/Desktop/mame-loshn/data/dataYiddish/songtexts/{letter}.txt') as f:
			text = f.read()
		trans = re.findall('    ([\w -]+?)&nbsp;', text)
		yiddish = re.findall('\+1>([\u0590-\u05FF].*?[\u0590-\u05FF]?)<', text)
	
		for tup in zip(yiddish, trans):
			print(tup)
			print()
			yiddishstrings.append(tup[0])
			translitstrings.append(tup[1])
	pd.DataFrame({'yiddish': yiddishstrings, 'translit': translitstrings, 'source': 'yidlid'}).to_csv(f'~/Desktop/mame-loshn/data/dataYiddish/songtexts/glossary.csv')
