#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import re
import pandas as pd

def parse_wikt(words):
	wordict = {}
	for word in words:
		url = f'https://en.wiktionary.org/wiki/{word}#Yiddish'
		wordpage = requests.get(url).text
		yiddishwords = re.findall('lang=\"yi\">.+?([\u0590-\u05FF]+)<', wordpage)
		latinwords = re.findall('lang=\"yi-Latn\".*?>(.+?)<', wordpage)
		for word1, word2 in zip(yiddishwords, latinwords):
			wordict[word1] = wordict[word2]	

	return wordict
