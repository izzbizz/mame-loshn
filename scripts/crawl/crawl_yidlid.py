#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import re
import pandas as pd

metaurl = 'http://yidlid.org/chansons/'
starturl = 'http://yidlid.org/chansons/index.html'

page = requests.get(starturl)
text = page.text
chansons = re.findall('\<table.+?=10\>([\w\W]+)\<\/table\>', text)
pairs = re.findall('\<a href="(.+?)"\>(.*?)<\/a>', chansons[0])
for pair in pairs:
	name = re.sub(' ', '-', pair[1])
	url = metaurl + pair[0]
	page = requests.get(url)
	text = page.text
	song = re.findall('\<table cellpadding=5\>([\w\W]+)\<\/table\>', text)
	lines = re.findall('\<tr\>([\w\W]+?)\<\/tr\>', song[0])
	with open(f'{name}.txt', 'w') as f:
		for line in lines:
			f.write(line)
	for line in lines:
		quartlets = re.findall('\<font size=.1\>([\w\W]+?)\<\/font', line)
	yiddish, trans, french, english = [], [], [], []
	try:
		yiddish.append(re.sub('[\n\r]', '', quartlets[0]))
		trans.append(re.sub('[\n\r]', '', quartlets[1]))
		french.append(re.sub('[\n\r]', '', quartlets[2]))
		english.append(re.sub('[\n\r]', '', quartlets[3]))
	except IndexError:
		pass
	pd.DataFrame({'yiddish': yiddish, 'transliterated': trans, 'french': french, 'english': english}).to_csv(f'~/Desktop/mame-loshn/data/dataYiddish/songtexts/{name}.csv')
