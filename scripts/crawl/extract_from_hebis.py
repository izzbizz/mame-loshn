#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lxml import etree
import glob
import json
import listdict
heb_chars = set("אבגדהוזחטיכךלמםנןסעפףקרשת")

def is_heb(string):
    return bool(heb_chars.intersection(string))


def get_paralles(latin, hebrew):
    out = []
    for key, lst in hebrew.items():
        for i, value in enumerate(lst):
            if is_heb(value):
                try:
                    out.append((latin[key][i], value))
                except (IndexError, KeyError):
                    pass
    return out


def process_fields(field_list):
    if not (field_list[0].get("U") and len(field_list) > 1):
        return None

    out = []
    while len(field_list) >= 2:
        first, second, *field_list = field_list
        try:
            script1 = listdict.getone(first, "U")
        except KeyError:
            script1 = "Latn"
        try:
            script2 = listdict.getone(second, "U")
        except KeyError:
            script2 = "Latn"
        if script1 == "Latn" and script2 == "Hebr":
            out.extend(get_paralles(first, second))
        elif script1 == "Hebr" and script2 == "Latn":
            out.extend(get_paralles(second, first))
    return out


def process_record(listdict_record):
    rec = listdict_record
    ppn = listdict.getone(rec, "003@", "0")
    out = {"ppn": ppn}
    for tag, fields in rec.items():
        optional_field = process_fields(fields)
        if optional_field:
            out[tag] = optional_field
    return out


def process_file(fh):
    root = etree.parse(fh)
    for xml_record in root.iter("record"):
        record = listdict.mk(
            xml_record.iter("datafield"),
            lambda field: (field.attrib["tag"], field.iter("subfield")),
            lambda subfield: (subfield.attrib["code"], subfield.text)
        )

        out = process_record(record)
        if out:
            print(json.dumps(out, ensure_ascii=False))


def main():
    for filename in glob.iglob("*.xml"):
        with open(filename) as fh:
            process_file(fh)


if __name__ == '__main__':
    main()
