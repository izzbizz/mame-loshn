#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import json
from fuzzywuzzy import fuzz
from operator import itemgetter
from collections import defaultdict

DATAPATH = '../../../data'

def align_row(yiddish, transliteration, letterdict, threshold=50, maxlen=1.5):
	'''align two strings using Levenshtein distance and a crude pseudo-transliteration'''
	wordlist = []
	matches = defaultdict(lambda: ('', 0),)
	for i, word in enumerate(yiddish.split()):
		pseudot = ''.join([letterdict[letter] for letter in word]) 
		ratios = []
		for j, t in enumerate(transliteration.split()):
			t_ = ''.join([letter for letter in t])
			if len(t_) <= len(pseudot)*maxlen and len(pseudot) <= len(t_)*maxlen:
				ratio = fuzz.ratio(pseudot, t_) - abs(i-j)
				ratios.append((t, pseudot, ratio))
		if len(ratios) > 0:
			maxratio = max(ratios, key=itemgetter(2))
			if maxratio[2] >= threshold:
				if matches[maxratio[0]][1] >= maxratio[2]:
					pass
				elif matches[maxratio[0]][1] < maxratio[2]:
					matches[maxratio[0]] = (word, maxratio[2])
	for key, val in matches.items():
		wordlist.append((val[0], key))
	return list(set(wordlist))
	
def add_to_pairs(row):
	parallels = list(zip(row.yiddish.split(), row.transliteration.split()))
	return [(pair[0], pair[1], row.source) for pair in parallels]
	
def align_comp(row, letterdict, maxlen):
	comp_dict = dict(zip(row.yiddish_comp.split(), row.yiddish.split()))
	alignments = align_row(row.yiddish_comp, row.transliteration, letterdict=letterdict, maxlen=maxlen)
	return [(comp_dict[alignment[0]], alignment[1], row.source) for alignment in alignments]

def align_all(f=f'{DATAPATH}/processed-combined/processed_all.csv', yivo=True, subsample_refoyl=False):
	df = pd.read_csv(f)
	df = df[df.yivo == yivo]
	if yivo:
		with open(f'{DATAPATH}/alignment_data/yivo_letterdict.json') as ifile:
			letterdict = json.load(ifile)
	letterdict = defaultdict(str, letterdict)
	df['y_len'] = df.yiddish_comp.apply(lambda x: len(x.split()))
	df['t_len'] = df.transliteration.apply(lambda x: len(x.split()))
	pairs = []
	for _, row in df[df.y_len == df.t_len].iterrows():
		parallelized = add_to_pairs(row)
		pairs += parallelized
	for _, row in df[df.y_len != df.t_len].iterrows():
		aligned = align_comp(row, letterdict, maxlen=1.7)
		pairs += aligned
	pairdf = pd.DataFrame({'yiddish': [pair[0] for pair in pairs], \
							'transliteration': [pair[1] for pair in pairs], \
							'source': [pair[2] for pair in pairs]})
	if subsample_refoyl:						
		pairdf = pd.concat([pairdf[pairdf.source != 'refoyl'], pairdf[pairdf.source == 'refoyl'].sample(30000)])
	pairdf = pairdf.drop_duplicates(['yiddish', 'transliteration'])
	pairdf = pairdf.sample(len(pairdf))
	return pairdf
	
def make_parallel_data_files(fname, yivo=True, subsample_refoyl=False):
	df = align_all(yivo=yivo, subsample_refoyl=subsample_refoyl)
	with open(f'../../../system/repo/data/{fname}', 'w') as f:
		for y, t in zip(df.yiddish.values, df.transliteration.values):
			f.write(f'{t}\t{y}\n')
	
if __name__ == '__main__':
	print('aligning yivo...\n')
	pairdf = align_all(subsample_refoyl=True)
	print(f'found {len(pairdf)} pairs\nprinting sample...\n')
	for _, row in pairdf.sample(2000).iterrows():
		print(f'Y: {row.yiddish[::-1]}\t, T: {row.transliteration}\t, S: {row.source}')
	print('saving file...')
	pairdf.to_csv(f'{DATAPATH}/processed-aligned/df.csv')
	
