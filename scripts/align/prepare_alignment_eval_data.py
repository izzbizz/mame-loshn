#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import random
import json
import sys

sys.path.append('../preprocess/')
from scramble_funcs import Scrambler

YIDDISH = 'yiddish_comp'
INFILE = '../../../data/processed-combined/processed_all2.csv'
OUTFILE = '../../../data/alignment_data/align_eval_yivo2.csv'

def prepare_eval_data(ifile, yivo=True, n=4):
	df = pd.read_csv(ifile)
	if yivo:
		'''use the perfectly aligned proverb texts to create evaluation material 
		for alignment strategies using the Scrambler''' 
		df = df[df.source == 'proverbs2']
		print(len(df))
		df['y_len'] = df.yiddish.apply(lambda x: len(x.split()))
		df['t_len'] = df.transliteration.apply(lambda x: len(x.split()))
		df = df[df.y_len == df.t_len]

	y_vocab = list(set([w for words in df[YIDDISH].values for w in words.split()]))
	t_vocab = list(set([w for words in df.transliteration.values for w in words.split()]))
		
	y_scrambler = Scrambler(y_vocab)
	t_scrambler = Scrambler(t_vocab)
	
	def make_eval_data(row, n):
		alignments = list(zip(row[YIDDISH].split(), row.transliteration.split()))
		modified_yiddish = y_scrambler.scramble(row[YIDDISH], n=n, delete=False)
		modified_transliteration = t_scrambler.scramble(row.transliteration, n=n)
		alignments = [pair for pair in alignments if pair[0] in modified_yiddish
						and pair[1] in modified_transliteration]
		return modified_yiddish, modified_transliteration, alignments	
	
	y, t, alignments = zip(*df.apply(lambda x: make_eval_data(x, n=n), axis=1))
	
	align_df = pd.DataFrame({'yiddish': y, 'transliteration': t, 
						 'alignments': [list(set(align)) for align in alignments]})
	return align_df
						 
if __name__ == '__main__':
	df = prepare_eval_data(INFILE, yivo=True, n=4)					 
	df.to_csv(OUTFILE, index=False)
