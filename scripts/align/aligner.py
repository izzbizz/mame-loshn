#!/usr/bin/env python
# -*- coding: utf-8 -*-

from fuzzywuzzy import fuzz
from operator import itemgetter
from collections import defaultdict
import deromanize as dr
import re
import copy
import yaml
import json

DICTPATH = '../../data/alignment_data/'

class Aligner():
	def __init__(self, dataset):
		self.LONG_WORD = 12
		self.MAXLEN = 3.0
		self.MAXLEN_LONGWORD = 1.8
		PROFILE = yaml.safe_load(open(DICTPATH+f'deromanizedict_{dataset}.yml'))
		self.keys = dr.KeyGenerator(PROFILE)
		self.deromdict = defaultdict(list)
		for l in self.keys.keys['base']:
			self.deromdict[l] = [x[1] for x in [v.keyvalue[0] for v in self.keys.keys['base'][l]]]
		self.deromkeys = set(self.deromdict.keys())
		with open(DICTPATH+f'firstletterdict_{dataset}.json') as f:
			firstletterdict = json.load(f)
		self.firstletterdict = defaultdict(list, firstletterdict)
	
	def _get_maxlen(self, pair):
		if len(pair[0]) > self.LONG_WORD \
		or len(pair[1]) > self.LONG_WORD:
			return self.MAXLEN_LONGWORD
		else:
			return self.MAXLEN
		
	def issame(self, pair):
		'''check if source and target word are likely to correspond
		by comparing their lengths and first letters'''
		maxlen = self._get_maxlen(pair)
		firstletter_y = pair[0][0]
		firstletter_t = pair[1][0]
		if firstletter_y in self.firstletterdict[firstletter_t] \
		and len(pair[0]) <= len(pair[1])*maxlen \
		and len(pair[1]) <= len(pair[0])*maxlen: 
			return True
		else: 
			return False

	def _max_match(self, yiddish, candidates):
		'''from the set of candidates for a word, return the 
		highest Levenshtein match'''
		ratios = []
		for c in candidates:
			ratios.append((yiddish, c, fuzz.ratio(yiddish,c)))
		try:
			return max(ratios, key=itemgetter(2))
		except ValueError:
			return ('', '', 0)
		
	def match_strings(self, yiddish, transliteration, threshold=80, top=100):
		'''check whether two strings align using deromanize'''
		tstring = ''.join([l for l in transliteration if l in self.deromkeys])
		parts = self.keys['base'].getallparts(tstring)
		pseudots = dr.add_rlists(parts)
		pseudots.sort()
		versions = [''.join([l[1] for l in w.keyvalue]) \
					for w in pseudots[:top]]
		bestmatch = self._max_match(yiddish, versions)
		if bestmatch[2] >= threshold:
			return True
		else:
			return False
			
	def check_giza(self, chunks, yiddish=True, thresh=40):
		aligned_pairs = set()
		unaligned_pairs = set()
		if yiddish:
			rgx = r'([\u0590-\u05FF\uFB2A-\uFB4E\-־–\']+) \(\{ ([\d+ ]+)'
		else:
			rgx = '([\w\-\u0323]+) \(\{ ([\d+ ]+)'
		for chunk in chunks:
			source_indices = [(k, [int(d)-1 for d in re.findall('\d+', v)]) for k, v in 
                   re.findall(rgx, chunk[1])]
			target_indices = dict(enumerate(chunk[0].split()))
			for tup in source_indices:
				if yiddish:
					source_word = tup[0]
					indices = tup[1]
					target_word = target_indices[indices[0]]
				else:
					target_word = tup[0]
					indices = tup[1]
					source_word = target_indices[indices[0]]
				if len(source_word) > self.LONG_WORD or len(target_word) > self.LONG_WORD:
					if self.issame((source_word, target_word)):
						aligned_pairs.add((source_word, target_word))
				elif self.match_strings(source_word, target_word, threshold=thresh) and \
				self.issame((source_word, target_word)):
					aligned_pairs.add((source_word, target_word))
				else:
					unaligned_pairs.add((source_word, target_word))
		return aligned_pairs, unaligned_pairs
