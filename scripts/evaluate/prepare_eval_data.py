#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import defaultdict
import pandas as pd
import sys
sys.path.append('../preprocess/')

from standardize_ortho import decompose_tsvey_yudn, decompose_vov_yud

def normalize_ortho(l):
	decomposed_yudn = [decompose_tsvey_yudn(w) for w in l]
	decomposed_vov_yud = [decompose_vov_yud(w) for w in decomposed_yudn]
	return decomposed_vov_yud
	
def prepare_predictions(predictionf, g2p=False):
	with open(predictionf) as f:
		lines = f.readlines()
		if g2p:
			inputs = [l.split('\t')[0] for l in lines]
			predictions = [l.split('\t')[1][:-1].replace(' ', '') for l in lines]
			predictions = normalize_ortho(predictions)
			predictions = list(zip(inputs, predictions))
		else:
			predictions = [l[:-1].replace('\u200f', '') for l in lines]
			predictions = normalize_ortho(predictions)
		return predictions

def prepare_t2t_predictions(predictionf, n=3):
	predictions = prepare_predictions(predictionf)
	pred_blocks = [predictions[i:i+n] for i in range(0, round(len(predictions)), n)]
	return [block[:n] for block in pred_blocks]
