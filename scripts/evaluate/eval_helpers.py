#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
from jiwer import wer

def dedup_test_data(test_data_file):
	with open(test_data_file) as f:
		lines = f.readlines()
	transliteration = [l.split('\t')[0] for l in lines]
	yiddish = [l.split('\t')[1][:-1] for l in lines]
	df = pd.DataFrame({'yiddish': yiddish, 'transliteration': transliteration})
	print(f'{len(df) word pairs in test data before deduplication'})
	df = df.drop_duplicates('transliteration')
	print(f'{len(df) word pairs in test data after deduplication'})
	with open(f'{test_data_file}_no_dup', 'w') as f:
    for y, t in zip(df.yiddish.values, df.transliteration.values):
        f.write(f'{t}\t{y}\n')
    with open(f'{test_data_file}_no_dup_inputs', 'w') as f:
    for t in df.transliteration.values:
        f.write(f'{t}\n')
    with open(f'{test_data_file}_no_dup_inputs', 'w') as f:
    for t in df.transliteration.values:
        f.write(f'{t}\n')

def custom_wer(row, n):
    for i in range(n):
        partial_wer = wer(row.gold, row[f'pred_{i+1}'])
        if partial_wer == 0:
            return partial_wer
    return partial_wer
