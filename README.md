# mame-loshn מאמע-לשון

This repository contains code, data and baseline models for my Master thesis project: Neural back-transliteration for Yiddish. "Back-transliteration" describes the task of converting Romanized data back into the original script (the [_alef-beys_](https://www.yiddishbookcenter.org/language-literature-culture/learn-yiddish-alphabet/alef-beys-chart), in this case). The actual neural Transformer models are contained in another location ([NETransliteration-COLING2018-fork](https://codeberg.org/izzbizz/NETransliteration-COLING2018-fork)), which is a fork from Merhav & Ash's repository whose code I used to train the models. To replicate all the results with the paths intact, both repositories should be cloned into the same directory.

This repository contains the following directories:
- `data`: two prepared datasets--one follows YIVO orthography, the second is a collection of metadata from four libraries, which follow different slightly different transliteration rules (as a shorthand, I call this the "_LC_" dataset, for "Library of Congress"). The data has been split into training, development and test data. 
- `hackathon`: legacy code for the original project (enrichment of Romanized metadata for the David Kohan [Jewish music archive](https://web.ub.uni-potsdam.de/kohanarchiv/index.php/Main_Page))
- `notebooks`: Juypter notebooks for data alignment, EDA and evaluation
- `scripts`: Python scripts, mostly for scraping, preprocessing, standardizing and aligning the data, and for evaluation

## Data
I obtained the data from the following sources:

- 11,000 records (titles and author names) scraped from the Yiddish Book Center's [Digital Yiddish Library](https://www.yiddishbookcenter.org/collections/digital-yiddish-library)
- 10,000 words from [Wiktionary](https://www.wiktionary.org/)
- 8,000 records provided by the [National Library of Israel](https://www.nli.org.il)
- 8,000 place names from [Yiddishland](https://yivo.org/yiddishland-topo)
- 7,000 records provided by the [Brandeis University library](https://www.brandeis.edu/library/)
- 2,700 nouns from the [Multi-Orthography Parallel Corpus of Yiddish Nouns](https://codeberg.org/jonne/yiddish-lrec-2020)
- 2,400 words from [Yidlid](http://yidlid.org/), a page with lyrics for yiddish songs
- 2,000 words from the [Yiddish Dictionary](http://www.yiddishdictionaryonline.com/beginner.htm)
- 1,500 records from the Hessisches Bibliotheksinformationssystem [hebis](https://www.hebis.de/)
- 1,300 names from Wikidata
- 300 proverbs from [Yiddish Wit](https://www.yiddishwit.com/List.html)

After preprocessing and an automated alignment process, I end up with two datasets: ~ 26,000 word-pairs for LC, ~130,000 word-pairs for YIVO. I downsample the latter to have roughly the same size as the first, to speed up training and to allow for a fair comparison of the two systems. The final raw parallel data can be found [here](data/lc) (LC) and [here](data/yivo_small) (YIVO) (Clicking on these might slow down your browser, as they are rather long text files).

## Baselines
I used the [_shraybmashinke_](http://www.cs.engr.uky.edu/~raphael/yiddish/makeyiddish.html) by Refoyl Finkl and the Sequitur [G2P system by Jonne Sälevä](https://codeberg.org/jonne/yiddish-lrec-2020) as baselines. I also used Sälevä's code to train another baseline system on each of the two sub-corpora.