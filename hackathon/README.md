## Project: Enrich David Kohan's Jewish Music Archive with Metadata

[Link to the archive](https://www.ub.uni-potsdam.de/de/ueber-uns/sammlungen/david-kohan-jewish-music-archive)

[Link to Wiki](https://web.ub.uni-potsdam.de/kohanarchiv/index.php/Main_Page) (only UP internal)

#### What's been done so far

- retrieve the available metadata via the MediaWiki API
- combine all ~11,000 files to one [pandas DataFrame](data/metadata.json)
- first [EDA](notebooks/EDA.ipynb): which values are missing, what do the data look like, duplicates?
- stored duplicate titles in a [csv file](data/duplicate_titles.csv)
- second overview: missing values per record/CD
- store CD information (percentage of missing values per field and record) in a [csv file](data/recordwise_values_percentage.csv)
- first attempt at cleaning up titles (some titles include information that belongs in a different field; that would obstruct automatic searches)
--> split into title, alternative titles, comment, whether the title is incomplete (True/False), whether the annotators were unsure (True/False)
- store modified information in a [json file](data/cleaned_titles.json)
- use modified titles as basis for automated [google search]() (search for title >> if YouTube link amongst first five links, return it (sample only because the search takes so long))

#### Further ideas (Master thesis?) 

- can we impute any values? e.g. 'Collector' can be used instead of missing titles, check whether titles on the same CD have any inherent connection...
- create and open-source a stopword list for Yiddish (it doesnt exist yet) using Wikipedia or smaller corpus?
- collect reputable resources for automatic web search
- refine the clean-titles script, do the same for performers
- identify more duplicates (for performers: levenshtein distance)
- identify yiddish v hebrew v slavic v ladino (?) v german titles
- train a translator for back-transliteration of Yiddish titles (latin to Hebrew letters) (train on Hebrew data or monolingual data first?)
- use information from WikiData to enrich data
- rank web search results according to relevance (parse websites and use our enriched metadata?)
- rank similarity of search and results

#### Libraries

- [mwclient](https://pypi.org/project/mwclient/)
- [googlesearch](https://github.com/MarioVilas/googlesearch)

#### Resources

- [parallel yiddish orthography corpus and translation task](https://www.aclweb.org/anthology/2020.lrec-1.119.pdf)
- [YIVO rules for transliteration](https://www.hagalil.com/jidish/yivo.htm)
- [Freedman Jewish Sound Archive](http://sceti.library.upenn.edu/freedman/contactus.cfm) -- the records have a very similar structure to Kohan's archive. "First line" is sometimes provided in original and transliterated Yiddish -- more training data?
([Example](http://digital.library.upenn.edu/webbin/freedman/lookupwork?hr=&what=Yoni%20Eilat%20%2F%20Tzigayner%20Neshume))
- [Yiddish Wikipedia](https://yi.wikipedia.org/wiki/)
- [Corpus of Modern Yiddish (CMY)](http://web-corpora.net/YNC/search/)

#### Reputable Websites

- [Israeli National Library](www.nli.org.il) -- has information on songs and recordings
