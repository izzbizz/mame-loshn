import glob
import re
import pandas as pd

def make_df():
	'''read in all the files and convert to key-value pairs
	to add to metadata dataframe'''

	files = glob.glob('../data/folders/CD*/*')

	df = pd.DataFrame(columns=['Title of Collector', 'Title', 'First Line', 'Performer', 'Instrumentation', 'Composer', 'Author', 'Genre', 'Language', 'Song Text', 'Translation', 'Duration', 'Year', 'Comment', 'Comment by David Kohan', 'Sources', 'Links'])

	for i, file in enumerate(files):
		with open(file, 'r') as f:
			text = f.read()
			items = text.split('\n==')
			pairs = [re.split('== ?\n', item) for item in items]
			d = {}
			for pair in pairs:
				key = re.sub('=', '', pair[0]).strip()
				if '/' in key:
					key = re.search('/ ?(.*?)', key)[1]
				try:
					d[key] = pair[1].strip()
				except IndexError:
					d[key] = None
			d['filename'] = file[-8:]
			df = df.append(d, ignore_index=True)

	df.to_json('../data/metadata.json')
	
if __name__ == '__main__':
	make_df()
