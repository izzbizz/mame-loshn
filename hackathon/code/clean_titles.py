import re

def clean_titles(title):
	if title in [None, '']:
		return '', [], '', False, False
	title_dict = {'title': title.strip(), 'alt_titles': [], 'title_comment': '', 'title_unsure': False, 'title_incomplete': False}
	title = title.strip()
	## this calls for a better solution (check if the entire entry is ONE paranthese)
	try:
		if title[0] == '(' and title[-1] == ')':
			title = title[1:-1]
			title_dict['title'] = title.strip()
	except IndexError:
		print(title)
	questionmarks = re.search('(\(?\?+\)?)', title)
	if bool(questionmarks):
		title = title.replace(questionmarks[1], '')
		title_dict['title'] = title.strip()
		title_dict['title_unsure'] = True
	trailingdots = re.search('\(?(\.\.+)\)?', title)
	if bool(trailingdots):
		title = title.replace(trailingdots[0], '')
		title_dict['title'] = title.strip()
		title_dict['title_incomplete'] = True
	comment = re.search('\(([\w\W]+?)\)', title)
	if comment:
		title = title.replace(comment[0], '')
		commentstring = comment[1]
		title_dict['title'] = title.strip()
		title_dict['title_comment'] = commentstring.strip()
	multipletitles = title.split('/')
	if len(multipletitles) > 1:
		title_dict['title'] = multipletitles[0]
		title_dict['alt_titles'] = [t.strip() for t in multipletitles[1:]]
	return title_dict['title'], title_dict['alt_titles'], title_dict['title_comment'], title_dict['title_unsure'], title_dict['title_incomplete']
	
if __name__ == '__main__':
	clean_titles()
