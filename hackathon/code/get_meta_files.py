import mwclient
import os

def get_meta_files():
	'''connect to MediaWiki and save track metadata as txt files'''

	site = mwclient.Site('web.ub.uni-potsdam.de', path='/kohanarchiv/')
	cdlist = site.categories['CDList']
	cdnames = [page.name for page in cdlist][1:]
	for name in cdnames:
		trunc_name = name[9:]
		os.mkdir('../data/{}/'.format(trunc_name))
		cd = site.categories[trunc_name]
		tracks = [page.name for page in cd]
		for track in tracks:
			entry = site.pages[track].text()
			with open('../data/{}/{}'.format(trunc_name, track), 'w') as f:
				f.write(entry)
				
if __name__ == '__main__':
	get_meta_files()
