from googlesearch import search
import time

def find_youtube_link(title, lang='en', stop=5):
	if len(title) == 0:
		return
	time.sleep(10) # add decorator
	sresults = search(title, stop=stop) 
	lresults = list(sresults)
	for link in lresults:
		if 'youtube' in link:
			return link
	return