## jupyter notebooks

This directory contains three types of notebooks:
- `EDA` shows some statistics about the corpus, grouped by source (e.g. number of records, tokens, average token length)
- `ALIGN_LC` and `ALIGN_YIVO` show the alignment procedure. Specifically `ALIGN_LC` samples from the selections to illustrate which pairs got discarded through the process, and which got added to the final dataset. It imports functions from [~/scripts/align](../scripts/align/)
- `EVALUATE_LC` and `EVALUATE_YIVO` evaluate the preformance of the baselines against the Transformer systems. Like the alignment notebooks, they include examples that serve to illustrate the behavior of the different systems. They import from [~/scripts/eval](../scripts/eval/)